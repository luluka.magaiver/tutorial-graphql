package br.com.marcusvinicuisgoliveira.tutorialgraphql;

/**
 * @author Marcus Vinicius
 * @created 19/01/2023 - 19:47
 * @project tutorial-graphql
 */
record Post(String id, String content) {

}
record Comment(String id, String content, String postId) {}
