package br.com.marcusvinicuisgoliveira.tutorialgraphql;

import org.springframework.graphql.data.method.annotation.SchemaMapping;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * @author Marcus Vinicius
 * @created 19/01/2023 - 19:49
 * @project tutorial-graphql
 */
@Service
class PostService {
    Map<String, Post> posts =new HashMap<>();

    Collection<Post> createPost(String content) {
        var newPost = new Post(UUID.randomUUID().toString(), content);

        posts.put(newPost.id(),newPost);

        return posts.values();
    }

    Post postById(String postId) {
        return posts.get(postId);
    }
}

@Service
class CommentService {
    Map<String, Comment> comments =new HashMap<>();

    Collection<Comment> createComment(String content, String postId) {
        var newComment = new Comment(UUID.randomUUID().toString(), content, postId);
        comments.put(newComment.id(), newComment);
        return comments.values();
    }

    public Collection<Comment> findByPost(String id) {
        return comments.values().stream().filter(comment -> comment.postId().equals(id)).collect(Collectors.toList());
    }
}
